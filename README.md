# waybar-config

My Waybar configuration

Requirements:
 - bash
 - fonts

## Instalation:

```sh
cd ~/.config
mv -v waybar waybar.bak # If you have your own config.
git clone https://codeberg.org/srijansengupta/waybar-config waybar
```

### Note:
 - For dwl, tags must be placed in to tags file, /tmp/dwltags-$USER where USER is your local user name and config.dwl must be used
 - For sway, config.sway must be symlinked to config
 - For wayfire, config.wayfire is the file to be used.
